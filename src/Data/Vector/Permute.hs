{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, GADTs, TypeOperators,
             FlexibleContexts, FlexibleInstances, TypeSynonymInstances
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Permute
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Compute various vector permutations.
-- 
-- Changelog:
--  + 20/01/2019  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Vector.Permute (sortperm) where

import Control.Monad.ST
import Data.Vector.Helpers (R, Z, len, efn, thw, frz)
import Data.Vector.Storable (Vector, Storable, (!))
-- import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Algorithms.Intro as Vec


-- * Compute permutations
------------------------------------------------------------------------------
-- | Compute the permutation that is required to sort that given list (in
--   ascending order).
--   
--   NOTE: use `Vec.backpermute` or `Vec.unsafeBackpermute` to perform the
--     permutation.
sortperm :: (Storable a, Ord a) => Vector a -> Vector Z
sortperm xs = runST $ do
  let n = len xs
      f i j = compare (xs!i) (xs!j)
  mj <- thw $ efn 0 n
  Vec.sortBy f mj
  frz mj
{-# SPECIALISE sortperm :: Vector R -> Vector Z #-}
{-# SPECIALISE sortperm :: Vector Z -> Vector Z #-}
{-# SPECIALISE sortperm :: Vector Float -> Vector Z #-}

{-# LANGUAGE BangPatterns, Rank2Types #-}
{-# LANGUAGE FlexibleContexts #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Helpers.Safe
-- Copyright   : (C) Patrick Suggate 2016
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Safe versions of the vector helper functions.
-- 
-- Changelog:
--  + 23/04/2016  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Vector.Helpers.Safe where

------------------------------------------------------------------------------
--
--  Operations useful for writing monadic, sparse-matrix code.
--
--  Changelog:
--   + 04/09/2015  --  initial file;
--

import GHC.Types (SPEC(..))
import Control.Monad.Primitive
import Data.Vector.Storable (Vector)
import Data.Vector.Storable.Mutable (MVector)
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as M


-- * Some convenient types.
------------------------------------------------------------------------------
-- | Default integral, and real, (-like) types.
type Z = Int
type R = Double

-- | Convenience aliases for some vector-types.
type BVec = Vector Bool
type IVec = Vector Int
type DVec = Vector Double
type FVec = Vector Float

-- | And some aliases for mutable vector-types.
type MVec  m a = MVector (PrimState m) a
type MVecB m   = MVector (PrimState m) Bool
type MVecI m   = MVector (PrimState m) Int
type MVecD m   = MVector (PrimState m) Double
type MVecF m   = MVector (PrimState m) Float


-- * Vector helpers.
------------------------------------------------------------------------------
infixl 9 !
(!) :: G.Vector v a => v a -> Int -> a
{-# INLINE (!) #-}
(!)  = (G.!)

len :: G.Vector v a => v a -> Int
len  = G.length
{-# INLINE len #-}

nul :: G.Vector v a => v a -> Bool
nul  = G.null
{-# INLINE nul #-}

vec :: G.Vector v a => [a] -> v a
vec  = G.fromList
{-# INLINE vec #-}

frz :: forall m v a. (PrimMonad m, G.Vector v a) =>
       G.Mutable v (PrimState m) a -> m (v a)
{-# INLINE frz #-}
frz  = G.freeze

thw :: forall m v a. (PrimMonad m, G.Vector v a) =>
       v a -> m (G.Mutable v (PrimState m) a)
{-# INLINE thw #-}
thw  = G.thaw

------------------------------------------------------------------------------
slc :: G.Vector v a => Int -> Int -> v a -> v a
{-# INLINE slc #-}
slc  = G.slice

tak :: G.Vector v a => Int -> v a -> v a
{-# INLINE tak #-}
tak  = G.take

drp :: G.Vector v a => Int -> v a -> v a
{-# INLINE drp #-}
drp  = G.drop

------------------------------------------------------------------------------
hed :: G.Vector v a => v a -> a
{-# INLINE hed #-}
hed  = G.head

lst :: G.Vector v a => v a -> a
{-# INLINE lst #-}
lst  = G.last

tal :: G.Vector v a => v a -> v a
{-# INLINE tal #-}
tal  = G.tail

nit :: G.Vector v a => v a -> v a
{-# INLINE nit #-}
nit  = G.init

------------------------------------------------------------------------------
bkp :: (G.Vector v Int, G.Vector v a) => v a -> v Int -> v a
{-# INLINE bkp #-}
bkp  = G.backpermute

upd_ :: (G.Vector v a, G.Vector v Int) => v a -> v Int -> v a -> v a
{-# INLINE upd_ #-}
upd_  = G.update_

acc_ :: (G.Vector v a, G.Vector v Int, G.Vector v b) =>
        (a -> b -> a) -> v a -> v Int -> v b -> v a
{-# INLINE acc_ #-}
acc_  = G.accumulate_


-- ** Constructors.
------------------------------------------------------------------------------
gen :: G.Vector v a => Z -> (Z -> a) -> v a
{-# INLINE[2] gen #-}
gen  = G.generate

efn :: (G.Vector v a, Num a) => a -> Z -> v a
{-# INLINE[2] efn #-}
efn  = G.enumFromN

rep :: G.Vector v a => Int -> a -> v a
{-# INLINE[2] rep #-}
rep  = G.replicate


-- * Monadic/mutable operations.
------------------------------------------------------------------------------
-- | Slice a mutable vector.
mlc :: M.MVector v a => Int -> Int -> v s a -> v s a
{-# INLINE mlc #-}
mlc  = M.slice

new :: (PrimMonad m, M.MVector v a) => Int -> m (v (PrimState m) a)
{-# INLINE new #-}
new  = M.new

infixl 2 `rd`

rd :: (PrimMonad m, M.MVector v a) => v (PrimState m) a -> Int -> m a
{-# INLINE rd #-}
rd  = M.read

wr :: (PrimMonad m, M.MVector v a) => v (PrimState m) a -> Int -> a -> m ()
{-# INLINE wr #-}
wr  = M.write

-- | Read, modify, and write.
mfy ::
  (PrimMonad m, M.MVector v a) => v (PrimState m) a -> (a -> a) -> Int -> m ()
{-# INLINE mfy #-}
mfy  = M.modify

------------------------------------------------------------------------------
-- | Vector copy functions.
vcp :: (PrimMonad m, G.Vector v a) => G.Mutable v (PrimState m) a -> v a -> m ()
{-# INLINE vcp #-}
vcp  = G.copy

mcp :: (PrimMonad m, M.MVector v a) =>
       v (PrimState m) a -> v (PrimState m) a -> m ()
{-# INLINE mcp #-}
mcp  = M.copy


-- * Functions often used when writing optimised vector functions.
------------------------------------------------------------------------------
-- | The version in the standard library is very slow, so probably doesn't
--   inline correctly?
whn :: Monad m => Bool -> m () -> m ()
{-# INLINE [0] whn #-}
whn b m = if b then m else return ()

-- | Like `when`, but with reversed call-ordering
whr :: Monad m => Bool -> m () -> m ()
{-# INLINE [0] whr #-}
whr False _ = return ()
whr _     m = m

-- | Break a vector into chunks.
chunk :: G.Vector v a => Int -> v a -> [v a]
{-# INLINE chunk #-}
chunk n =
  let go !_ !w | n < len w = (tak n w):go SPEC (drp n w)
               | otherwise = [w]
  in  go SPEC

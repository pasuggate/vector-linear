{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns, RecordWildCards,
             MultiParamTypeClasses, FunctionalDependencies,
             FlexibleInstances, TypeSynonymInstances,
             GeneralizedNewtypeDeriving,
             StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             CPP, TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Linear
-- Copyright   : (C) Patrick Suggate 2016
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Miscellaneous vector linear-algebra functions.
--
-- NOTE:
-- 
-- Changelog:
--  + 13/09/2016  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Vector.Linear
       ( module Help
       , Outerspace (..)
       , tensorprod
       , otter
       , otWith
       , vscl
       , mkVec
       , mkv1
       , unv1
       ) where

import GHC.Types (SPEC(..))
-- import Control.Monad
import Control.Monad.ST
-- import Control.Monad.Primitive
import Control.Lens ((^.), (.~)) -- , (%~))
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec
import Linear
import Data.Vector.Helpers as Help


-- * Outer-product functionality.
------------------------------------------------------------------------------
-- | Compute the outer-product of a vector of arrays, yielding an array of
--   vectors.
class (Foldable f, Applicative f) => Outerspace f where
  outerspace :: (Storable a, Storable (f a)) => f (Vector a) -> Vector (f a)


-- ** Some instances.
------------------------------------------------------------------------------
instance Outerspace V1 where
  {-# INLINE outerspace #-}
  outerspace (V1 x) = Vec.map V1 x

instance Outerspace V2 where
  {-# INLINE outerspace #-}
  outerspace (V2 x y) = otWith V2 x y

instance Outerspace V3 where
  {-# INLINE outerspace #-}
  outerspace (V3 x y z) = otWith f3 x $ otWith V2 y z
    where
      f3 = flip (_yz .~) . pure

instance Outerspace V4 where
  {-# INLINE outerspace #-}
  outerspace (V4 x y z w) = otWith f4 x $ outerspace (V3 y z w)
    where
      f4 = flip (_yzw .~) . pure


-- * Miscellaneous outer-product functions.
------------------------------------------------------------------------------
-- | Compute the otter-product of the two given vectors.
otter :: (Storable a, Num a) => Vector a -> Vector a -> Vector a
{-# INLINE otter #-}
otter vs ws = runST $ do
  let nz = len vs*w
      w  = len ws
  ar <- new nz
  let row !v !j !q
        | j  <  w   = wr ar q (v*ws!j) *> row v (j+1) (q+1)
        | otherwise = return q
      go !i !q
        | q  <  nz  = row (vs!i) 0 q >>= go (i+1)
        | otherwise = frz ar
  go 0 0

otWith :: (Storable a, Storable b, Storable c) =>
          (a -> b -> c) -> Vector a -> Vector b -> Vector c
{-# INLINE otWith #-}
otWith f vs ws = runST $ do
  let nz = len vs*w
      w  = len ws
  ar <- new nz
  let row !_ !v !j !q
        | j  <  w   = wr ar q (f v $ ws!j) *> row SPEC v (j+1) (q+1)
        | otherwise = return q
      go !_ !i !q
        | q  <  nz  = row SPEC (vs!i) 0 q >>= go SPEC (i+1)
        | otherwise = frz ar
  go SPEC 0 0

------------------------------------------------------------------------------
-- | Generic tensor-product.
--   NOTE: Assumes column-major ordering.
tensorprod :: (Storable a, Foldable f, Num a) => f (Vector a) -> Vector a
{-# INLINE tensorprod #-}
tensorprod  = foldr1 otter


-- * Miscellaneous helper functions.
------------------------------------------------------------------------------
mkv1 :: Storable a => Vector a -> Vector (V1 a)
{-# INLINE mkv1 #-}
mkv1  = Vec.map V1

unv1 :: Storable a => Vector (V1 a) -> Vector a
{-# INLINE unv1 #-}
unv1  = Vec.map (^._x)

------------------------------------------------------------------------------
-- | Zipwith multiply.
vscl :: (Additive f, Num a) => f a -> f a -> f a
{-# INLINE vscl #-}
vscl  = liftI2 (*)


-- ** Additional coercion helper-functions.
------------------------------------------------------------------------------
-- | Convert the given storable container to a storable vector.
mkVec :: (Storable (f a), Storable a) => f a -> Vector a
{-# INLINABLE[1] mkVec #-}
{-# SPECIALISE   mkVec :: V1 R -> Vector R #-}
{-# SPECIALISE   mkVec :: V2 R -> Vector R #-}
{-# SPECIALISE   mkVec :: V3 R -> Vector R #-}
{-# SPECIALISE   mkVec :: V4 R -> Vector R #-}
mkVec  = Vec.unsafeCast . Vec.singleton

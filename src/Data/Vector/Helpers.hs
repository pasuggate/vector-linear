{-# LANGUAGE CPP #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Helpers
-- Copyright   : (C) Patrick Suggate 2016
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Some convenient function aliases for working with the `vector` library.
-- 
-- Changelog:
--  + 04/09/2011  --  initial file;
--  + 20/05/2016  --  refactored to support either safe or unsafe versions;
-- 
------------------------------------------------------------------------------

module Data.Vector.Helpers
#ifdef __SAFE_INDEXING
       (module Data.Vector.Helpers.Safe) where

import Data.Vector.Helpers.Safe
#else
       (module Data.Vector.Helpers.Unsafe) where

import Data.Vector.Helpers.Unsafe
#endif

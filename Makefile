all:	doc

doc:	README.pdf

build:
	@cabal build -j

clean:
	@cabal clean
	@rm README.pdf

%.pdf: %.md
	@pandoc README.md -o README.pdf

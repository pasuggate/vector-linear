{-# LANGUAGE Arrows, TypeOperators, PatternSynonyms, TupleSections,
             FlexibleContexts, BangPatterns,
             FunctionalDependencies,
             FlexibleInstances,
             GeneralizedNewtypeDeriving,
             StandaloneDeriving,
             DeriveFunctor, DeriveFoldable, DeriveTraversable, DeriveGeneric,
             GADTs, KindSignatures, DataKinds, PolyKinds,
             TypeFamilies, RankNTypes, ScopedTypeVariables,
             UndecidableInstances,
             TemplateHaskell
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Matrix.Sized
-- Copyright   : (C) Patrick Suggate 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Miscellaneous, type-level-sized matrices and linear-algebra functions.
--
-- NOTE:
-- 
-- Changelog:
--  + 12/02/2020  --  initial file;
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.Matrix.Sized
  ( M16 (..)
  , m16
  , outerWith
  , outerProd
  ) where

import GHC.Generics (Generic)
import Control.Lens (makeLenses)
import Data.VectorSpace
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec

import Data.Vector.Extras


{-- }
class Outerable x t | t -> x where
  type OuterOf t :: * -> *
  outerProd :: x a => t a -> t a -> t a
  outerWith :: (a -> b -> c) -> t a -> t b -> OuterOf t c
--}

newtype M16 a = M16 { _m16 :: Vector a }
  deriving (Eq, Show, Generic)


-- * Lenses and instances
------------------------------------------------------------------------------
makeLenses ''M16

-- ** Vector-space instances
------------------------------------------------------------------------------
instance (Storable a, AdditiveGroup a) => AdditiveGroup (M16 a) where
  zeroV           = M16 $ rep 256 zeroV
  negateV         = M16 . Vec.map negateV . _m16
  M16 a ^+^ M16 b = M16 $ Vec.zipWith (^+^) a b
  {-# INLINE zeroV   #-}
  {-# INLINE negateV #-}
  {-# INLINE (^+^)   #-}

instance (Storable a, VectorSpace a) => VectorSpace (M16 a) where
  type Scalar (M16 a) = Scalar a
  s *^ M16 xs = M16 $ Vec.map (s*^) xs
  {-# INLINE (*^)    #-}


-- * Vector-matrix operations
------------------------------------------------------------------------------
outerWith ::
  (Storable a, Storable b, Storable c) =>
  (a -> b -> c) -> V16 a -> V16 b -> M16 c
outerWith f (V16 a) (V16 b) = M16 $ otWith f a b
{-# INLINE outerWith #-}

outerProd :: (Storable a, Num a) => V16 a -> V16 a -> M16 a
outerProd (V16 a) (V16 b) = M16 $ otter a b
{-# INLINE outerProd #-}


-- * Conversions
------------------------------------------------------------------------------

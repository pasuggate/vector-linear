{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies, GADTs, CPP,
             MultiParamTypeClasses, FlexibleContexts, FlexibleInstances,
             DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
             ViewPatterns, PatternSynonyms, TupleSections, OverloadedLists,
             StandaloneDeriving, GeneralizedNewtypeDeriving, InstanceSigs,
             TypeSynonymInstances, ScopedTypeVariables, TemplateHaskell,
             BangPatterns, FunctionalDependencies, UndecidableInstances
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Sized
-- Copyright   : (C) Patrick Suggate, 2020
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Some sized vector data-types, to allow for better storable and vector-space
-- instances.
-- 
-- Changelog:
--  + 12/01/2020  --  initial file;
-- 
------------------------------------------------------------------------------

module Data.Vector.Sized
  ( V16 (..)
  , v16
  , V64 (..)
  , v64
  ) where

import GHC.Generics (Generic)
-- import GHC.TypeNats
import GHC.Exts (IsList (..))
-- import Foreign.C.Types
import Foreign.Ptr
import Foreign.Marshal.Array (peekArray, pokeArray)
-- import Foreign.Marshal.Utils (with)
import Foreign.Storable
-- import Control.Arrow ((<<<), (***), (&&&))
import Control.Lens (makeLenses, (%~))
-- import Data.Proxy (Proxy (..))
-- import Data.Foldable (foldl')
import Data.MonoTraversable
import Data.VectorSpace
import Data.Vector.Helpers
import Data.Vector.Storable (Vector)
import qualified Data.Vector.Storable as Vec
-- import Text.Printf
-- import Data.Vector.Linear (otter, otWith)


-- ** Sized-vector data types
------------------------------------------------------------------------------
-- | For representing the basis-function contributions of 2D cubic B-splines.
newtype V16 a = V16 { _v16 :: Vector a }
  deriving (Eq, Show, Generic, MonoFunctor, MonoFoldable)

-- | For representing the basis-function contributions of 3D cubic B-splines.
newtype V64 a = V64 { _v64 :: Vector a }
  deriving (Eq, Show, Generic, MonoFunctor, MonoFoldable)

-- ** Sized-arrays for convenience
------------------------------------------------------------------------------
type instance Element (V16 a) = a
type instance Element (V64 a) = a


-- * Quadrature lenses and instances
------------------------------------------------------------------------------
makeLenses ''V16
makeLenses ''V64

-- ** Vector-space instances
------------------------------------------------------------------------------
instance (Num a, Storable a) => AdditiveGroup (V16 a) where
  zeroV           = V16 $ 16 `rep` 0
  negateV         = v16 %~ Vec.map negate
  V16 a ^+^ V16 b = V16 $ Vec.zipWith (+) a b
  V16 a ^-^ V16 b = V16 $ Vec.zipWith (-) a b
  {-# INLINE zeroV   #-}
  {-# INLINE negateV #-}
  {-# INLINE (^+^)   #-}
  {-# INLINE (^-^)   #-}

instance (Num a, Storable a) => AdditiveGroup (V64 a) where
  zeroV           = V64 $ 64 `rep` 0
  negateV         = v64 %~ Vec.map negate
  V64 a ^+^ V64 b = V64 $ Vec.zipWith (+) a b
  V64 a ^-^ V64 b = V64 $ Vec.zipWith (-) a b
  {-# INLINE zeroV   #-}
  {-# INLINE negateV #-}
  {-# INLINE (^+^)   #-}
  {-# INLINE (^-^)   #-}

------------------------------------------------------------------------------
instance (Num a, Storable a) => VectorSpace (V16 a) where
  type Scalar (V16 a) = a
  x *^ V16 xs = V16 $ Vec.map (x*) xs
  {-# INLINE (*^) #-}

instance (Num a, Storable a) => VectorSpace (V64 a) where
  type Scalar (V64 a) = a
  x *^ V64 xs = V64 $ Vec.map (x*) xs
  {-# INLINE (*^) #-}

-- ** Storable instances
------------------------------------------------------------------------------
instance forall a. Storable a => Storable (V16 a) where
  sizeOf  _ = 16*sizeOf (undefined :: a)
  alignment = const $ alignment (undefined :: a)
  peek p    = V16 . Vec.fromList <$> peekArray 16 (castPtr p :: Ptr a)
  poke p    = pokeArray (castPtr p :: Ptr a) . Vec.toList . _v16
  {-# INLINE sizeOf    #-}
  {-# INLINE alignment #-}
  {-# INLINE peek      #-}
  {-# INLINE poke      #-}

instance forall a. Storable a => Storable (V64 a) where
  sizeOf  _ = 64*sizeOf (undefined :: a)
  alignment = const $ alignment (undefined :: a)
  peek p    = V64 . Vec.fromList <$> peekArray 64 (castPtr p :: Ptr a)
  poke p    = pokeArray (castPtr p :: Ptr a) . Vec.toList . _v64
  {-# INLINE sizeOf    #-}
  {-# INLINE alignment #-}
  {-# INLINE peek      #-}
  {-# INLINE poke      #-}

-- ** List-like instances
------------------------------------------------------------------------------
instance Storable a => IsList (V16 a) where
  type Item (V16 a) = a
  fromList     = V16 . Vec.fromList
  fromListN 16 = fromList
  fromListN  _ = error "Only vectors of length 16 are allowed."
  toList       = Vec.toList . _v16
  {-# INLINE fromList  #-}
  {-# INLINE fromListN #-}
  {-# INLINE toList    #-}

instance Storable a => IsList (V64 a) where
  type Item (V64 a) = a
  fromList     = V64 . Vec.fromList
  fromListN 64 = fromList
  fromListN  _ = error "Only vectors of length 64 are allowed."
  toList       = Vec.toList . _v64
  {-# INLINE fromList  #-}
  {-# INLINE fromListN #-}
  {-# INLINE toList    #-}

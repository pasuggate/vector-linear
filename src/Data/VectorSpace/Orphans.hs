{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, GADTs, TypeOperators,
             OverloadedLists, TupleSections, BangPatterns, InstanceSigs,
             MultiParamTypeClasses, TypeFamilies, FunctionalDependencies,
             FlexibleContexts, FlexibleInstances, TypeSynonymInstances,
             DeriveGeneric, DeriveFunctor, DeriveFoldable, DeriveTraversable,
             StandaloneDeriving, GeneralizedNewtypeDeriving,
             ViewPatterns, PatternSynonyms, ScopedTypeVariables,
             TemplateHaskell
  #-}
------------------------------------------------------------------------------
-- |
-- Module      : Data.VectorSpace.Orphans
-- Copyright   : (C) Patrick Suggate, 2018
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Orphan instances for Conal's `vector-space` library.
-- 
-- Changelog:
--  + 24/11/2018  --  initial file;
-- 
-- TODO:
--  + move to `vector-linear`?
-- 
------------------------------------------------------------------------------

module Data.VectorSpace.Orphans where

import GHC.Int
import Data.VectorSpace
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Storable as S
import Data.Vector.Helpers
-- import Linear (V1 (..), V2 (..), V3 (..), V4 (..))
import Linear.Handy hiding ((*^), (^*), (^+^))


-- * Orphan instances
------------------------------------------------------------------------------
instance AdditiveGroup Int8 where
  zeroV   = 0
  a ^+^ b = a + b
  a ^-^ b = a - b
  negateV = negate
  {-# INLINE zeroV   #-}
  {-# INLINE (^+^)   #-}
  {-# INLINE (^-^)   #-}
  {-# INLINE negateV #-}

instance AdditiveGroup Int16 where
  zeroV   = 0
  a ^+^ b = a + b
  a ^-^ b = a - b
  negateV = negate
  {-# INLINE zeroV   #-}
  {-# INLINE (^+^)   #-}
  {-# INLINE (^-^)   #-}
  {-# INLINE negateV #-}

instance AdditiveGroup Int32 where
  zeroV   = 0
  a ^+^ b = a + b
  a ^-^ b = a - b
  negateV = negate
  {-# INLINE zeroV   #-}
  {-# INLINE (^+^)   #-}
  {-# INLINE (^-^)   #-}
  {-# INLINE negateV #-}

instance AdditiveGroup Int64 where
  zeroV   = 0
  a ^+^ b = a + b
  a ^-^ b = a - b
  negateV = negate
  {-# INLINE zeroV   #-}
  {-# INLINE (^+^)   #-}
  {-# INLINE (^-^)   #-}
  {-# INLINE negateV #-}

-- ** Compound orphan instances
------------------------------------------------------------------------------
instance AdditiveGroup a => AdditiveGroup (V1 a)
instance AdditiveGroup a => AdditiveGroup (V2 a)
instance AdditiveGroup a => AdditiveGroup (V3 a)
instance AdditiveGroup a => AdditiveGroup (V4 a)
instance AdditiveGroup a => AdditiveGroup (V5 a)
instance AdditiveGroup a => AdditiveGroup (V6 a)


-- * Orphan instances
------------------------------------------------------------------------------
{-- }
-- | `AdditiveGroup` instances for the fixed-size vectors from `Linear`.
instance AdditiveGroup (V1 R)
instance AdditiveGroup (V2 R)
instance AdditiveGroup (V3 R)
instance AdditiveGroup (V4 R)
--}

-- | `VectorSpace` instances for the fixed-size vectors from `Linear`.
instance VectorSpace   (V1 R)
instance VectorSpace   (V2 R)
instance VectorSpace   (V3 R)
instance VectorSpace   (V4 R)

-- | `InnerSpace` instances for the fixed-size vectors from `Linear`.
instance InnerSpace    (V1 R)
instance InnerSpace    (V2 R)
instance InnerSpace    (V3 R)
instance InnerSpace    (V4 R)

-- ** And instances for matrices
------------------------------------------------------------------------------
{-- }
instance AdditiveGroup (M22 R)
instance AdditiveGroup (M23 R)
instance AdditiveGroup (M24 R)

instance AdditiveGroup (M32 R)
instance AdditiveGroup (M33 R)
instance AdditiveGroup (M34 R)

instance AdditiveGroup (M42 R)
instance AdditiveGroup (M43 R)
instance AdditiveGroup (M44 R)
--}

------------------------------------------------------------------------------
instance VectorSpace   (M22 R)
instance VectorSpace   (M23 R)
instance VectorSpace   (M24 R)

instance VectorSpace   (M32 R)
instance VectorSpace   (M33 R)
instance VectorSpace   (M34 R)

instance VectorSpace   (M42 R)
instance VectorSpace   (M43 R)
instance VectorSpace   (M44 R)

------------------------------------------------------------------------------
{-- }
instance (S.Storable a, Num a) => AdditiveGroup (S.Vector a) where
  zeroV = S.empty
  a ^+^ b
    | S.null a  = b
    | S.null b  = a
    | otherwise = S.zipWith (+) a b
  negateV = S.map negate
  {-# INLINE zeroV #-}
  {-# INLINE (^+^) #-}
  {-# INLINE negateV #-}

-- instance (S.Storable a, AdditiveGroup (S.Vector a)) => VectorSpace (S.Vector a) where
instance (S.Storable a, Num a) => VectorSpace (S.Vector a) where
  type Scalar (S.Vector a) = a
  (*^) = S.map . (*)
  {-# INLINE (*^) #-}

instance (S.Storable a, AdditiveGroup a, Num a) => InnerSpace (S.Vector a) where
  a <.> b
    | S.null a  = zeroV
    | S.null b  = zeroV
    | otherwise = S.foldl1' (+) $ S.zipWith (*) a b
  {-# INLINE (<.>) #-}
--}

{-- }
-- instance G.Vector v R => AdditiveGroup (v R)
-- instance G.Vector v R => VectorSpace   (v R)
instance AdditiveGroup (S.Vector R) where
  zeroV = S.empty
  (^+^) = S.zipWith (+)
  negateV = S.map negate
  {-# INLINE zeroV #-}
  {-# INLINE (^+^) #-}
  {-# INLINE negateV #-}

instance VectorSpace (S.Vector R) where
  type Scalar (S.Vector R) = R
  (*^) = S.map . (*)
  {-# INLINE (*^) #-}
--}

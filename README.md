---
title: "README for `vector-linear`"
author: Patrick Suggate <<patrick.suggate@gmail.com>>
date: 8^th^ February, 2020
colorlinks: true
#toc: true
#toc-depth: 2
#toccolor: purple
geometry: margin=2cm
fontsize: 11pt
papersize: a4
...

# Introduction #

Convenience functions and data types for working with [`vector`](https://hackage.haskell.org/package/vector) arrays -- along with [`linear`](https://hackage.haskell.org/package/linear) and [`vector-space`](https://hackage.haskell.org/package/vector-space) abstractions for these. The `Data.Vector.Helpers`{.haskell} module defines numerous (shorthand) aliases for functions from [`vector`](https://hackage.haskell.org/package/vector), as these can be tedious to write out over-and-over. For example:

```haskell
  rep n x  ===  n `rep` x  ===  Data.Vector[.Storable|.Unboxed].replicate n x
```

Also included are some [`vector-space`](https://hackage.haskell.org/package/vector-space) orphan-instances for various types not included within the original library.

# Modules #

The following modules are compiled and exported:

```haskell
  -- top-level (re-)exports:
  Data.Vector.Extras

  -- convenience alias for @Data.Vector@:
  Data.Vector.Helpers
  Data.Vector.Helpers.Safe
  Data.Vector.Helpers.Unsafe

  -- combines `linear` and `vector` stuff:
  Data.Vector.Linear
  Data.Vector.Permute
  Data.Vector.Sized

  -- just a bunch of extra, orphan instances:
  Data.VectorSpace.Orphans
```

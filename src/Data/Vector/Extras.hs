{-# LANGUAGE DataKinds, PolyKinds, KindSignatures, TypeFamilies,
             BangPatterns
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.Vector.Extras
-- Copyright   : (C) Patrick Suggate, 2019
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Re-exporter module for a bunch of handy vector types, and functions.
-- 
-- Changelog:
--  + 08/06/2019  --  initial file;
-- 
------------------------------------------------------------------------------

module Data.Vector.Extras
  ( module Data.Vector.Helpers
  , module Data.Vector.Permute
  , module Data.Vector.Linear
  , module Data.Vector.Sized
  , bsearch
  ) where

import Data.Bits (unsafeShiftR)
import Data.Vector.Storable (Storable, Vector)

import Data.Vector.Helpers
import Data.Vector.Permute
import Data.Vector.Linear
import Data.Vector.Sized


-- * Extra functions
------------------------------------------------------------------------------
-- | Find the index of the interval that contains the given value, within the
--   array of monotone-increasing values representing intervals.
--   
--   TODO: testing.
--   
bsearch :: (Storable a, Ord a) => Vector a -> a -> Int
bsearch xs x =
  let n = len xs
      go !l !m !h
        | x  < xs!m     = go l ((l+m) `unsafeShiftR` 1) m
        | x >= xs!(m+1) = go m ((m+h) `unsafeShiftR` 1) h
        | otherwise     = m
  in  go 0 (n `unsafeShiftR` 1) (n - 1)
{-# INLINE bsearch #-}
